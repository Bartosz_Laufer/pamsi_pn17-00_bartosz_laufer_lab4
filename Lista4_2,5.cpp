#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

template <typename Typ>
class drzewo;

template <typename Typ>
class wezel {
public: 
	wezel *ojciec; // wskaznik na ojca
	wezel **synowie; // tablica wskazniko na synow
	int liczbaSynow; // liczba aktualnie posiadanych synow przez wezel
	int pojemnosc; // aktualny rozmiar tablicy
	Typ wartosc; // wartosc przechowywana w wezle
	wezel(); // konstruktor
	bool czyLisc(); // sprawdza czy wezel jest lisciem
	int LczyP;
	friend class drzewo<Typ>;
};

template <typename Typ>
wezel<Typ>::wezel()
{
	ojciec = nullptr;
	synowie = new wezel*[2];
	synowie[0] = nullptr;
	synowie[1] = nullptr;
	liczbaSynow = 0;
	pojemnosc = 2;
	LczyP = 2;
}

template <typename Typ>
bool wezel<Typ>::czyLisc()
{
	if (synowie[0] == nullptr && synowie[1] == nullptr)
		return true;
	else
		return false;
}

template <typename Typ>
class drzewo {
public: 
	drzewo();
	void dodaj(Typ wartosc, wezel<Typ> *temp); // dodaje element do drzewa
	void dodajEl(wezel<Typ> *node, wezel<Typ> *nowy, int ktorySyn); // funkcja pomocnicza do dodawania 
	void usun(Typ wartosc, wezel<Typ> *temp);
	void usunEl(wezel<Typ> *node, int ktorySyn);
	wezel<Typ> *Korzen(); // zwraca wskaznik na korzen
	Typ KorzenW(); // zwraca wartosc korzenia
	void preOrder(); // przejscie drzewa wzdluzne
	void postOrder(); // przejscie drzewa wsteczne
	void inOrder(); // przejscie drzewa poprzecznie
	void usunDrzewo(wezel<Typ> *wezel);
private:
	wezel<Typ> *korzen;
	void preOrder(wezel<Typ> *wezel); // metoda pomocnicza do przejscia drzewa
	void postOrder(wezel<Typ> *wezel); // metoda pomocnicza do przejscia drzewa
	void inOrder(wezel<Typ> *wezel); // metoda pomocnicza do przejscia drzewa
};

template <typename Typ>
drzewo<Typ>::drzewo()
{
	korzen = nullptr;
}

template<typename Typ>
wezel<Typ>* drzewo<Typ>::Korzen()
{
	return korzen;
}

template<typename Typ>
Typ drzewo<Typ>::KorzenW()
{
	return korzen->wartosc;
}

template <typename Typ>
void drzewo<Typ>::dodajEl(wezel<Typ> *node, wezel<Typ> *nowy, int ktorySyn)
{
	node->synowie[ktorySyn] = nowy; // ustawienie nowego wezla
	node->synowie[ktorySyn]->ojciec = node;
	node->liczbaSynow++;
	node->synowie[ktorySyn]->LczyP = ktorySyn; // przypisanie wartosci 0 lub 1, w zaleznosci od tego czy jest lewy czy prawy
}

template <typename Typ>
void drzewo<Typ>::dodaj(Typ wartosc, wezel<Typ> *temp)
{
	wezel<Typ> *nowy = new wezel<Typ>;
	nowy->wartosc = wartosc; // nadanie nowemu wezlowi wartosci
	if (korzen == nullptr)
		korzen = nowy; // jesli nie ma korzenia, to nowy wezel zostaje korzeniem
	else {
		int i = rand() % 2;
		if (temp->synowie[i] != nullptr) { // funkcja idzie dalej, jesli jeszcze nie dotarla do nullptr
			temp = temp->synowie[i];
			dodaj(wartosc, temp);
		}
		else {
			dodajEl(temp, nowy, i); // dodawanie elementu, gdy funkcja doszla do konca drzewa
		}
	}
}

template <typename Typ>
void drzewo<Typ>::usunEl(wezel<Typ> *node, int ktorySyn)
{
	if (node->czyLisc()) { // gdy wezel jest lisciem 
		node->ojciec->synowie[ktorySyn] = nullptr; // usuniecie syna
		node->ojciec->liczbaSynow--;
	}
	else if (node->synowie[0] != nullptr && node->synowie[1] == nullptr) { // gdy wezel ma jednego syna
		node->synowie[0]->ojciec = node->ojciec; // ojcem bedzie teraz dziadek
		node->ojciec->synowie[ktorySyn] = node->synowie[0]; // synem bedzie teraz wnuk
		node->ojciec->synowie[ktorySyn]->LczyP = ktorySyn;
	}
	else if (node->synowie[0] == nullptr && node->synowie[1] != nullptr) { // gdy wezel ma jednego syna
		node->synowie[1]->ojciec = node->ojciec; // ojcem bedzie teraz dziadek
		node->ojciec->synowie[ktorySyn] = node->synowie[1]; // synem bedzie teraz wnuk
		node->ojciec->synowie[ktorySyn]->LczyP = ktorySyn;
	}
	else { // gdy wezel ma dwoch synow
		node->ojciec->synowie[ktorySyn] = nullptr; // "uciecie" galezi
		node->ojciec->liczbaSynow--;
		node->ojciec = nullptr;
	}
}

template <typename Typ>
void drzewo<Typ>::usun(Typ wartosc, wezel<Typ> *temp)
{
	if (korzen == nullptr)
		cout << "Nie ma zadnych elementow w drzewie!\n"; // jesli nie ma korzenia, to nowy wezel zostaje korzeniem
	else {
		if (temp->wartosc == wartosc) usunEl(temp, temp->LczyP);
		if (temp->synowie[0] != nullptr)
			usun(wartosc, temp->synowie[0]);
		if (temp->synowie[1] != nullptr)
			usun(wartosc, temp->synowie[1]);
	}
}

template <typename Typ>
void drzewo<Typ>::usunDrzewo(wezel<Typ> *wezel)
{
	if (wezel != nullptr) { // sprawdza czy wezel jest lisciem
		for (int i = 0; i < wezel->liczbaSynow; i++) {
			usunDrzewo(wezel->synowie[i]);
		}
		delete wezel;
	}
}

template <typename Typ>
void drzewo<Typ>::preOrder(wezel<Typ> *wezel)
{ // wezel odwiedzany przed jego potomkami
	cout << wezel->wartosc << " "; // wyswietlenie wartosci elementu
	if (wezel->synowie[0] != nullptr)
		preOrder(wezel->synowie[0]);
	if (wezel->synowie[1] != nullptr)
		preOrder(wezel->synowie[1]);
	//if (wezel->czyLisc())
	//cout << endl; // od nowej linii gdy natrafiono na lisc
}

template <typename Typ>
void drzewo<Typ>::preOrder()
{
	preOrder(Korzen());
}

template <typename Typ>
void drzewo<Typ>::postOrder(wezel<Typ> *wezel)
{ // wezel jest odwiedzany po jego potomkach
	if (wezel->synowie[0] != nullptr)
		postOrder(wezel->synowie[0]);
	if (wezel->synowie[1] != nullptr)
		postOrder(wezel->synowie[1]);
	cout << wezel->wartosc << " "; // wyswietlenie wartosci elementu
}

template <typename Typ>
void drzewo<Typ>::postOrder()
{
	postOrder(Korzen());
}

template <typename Typ>
void drzewo<Typ>::inOrder(wezel<Typ> *wezel)
{ // wezel odwiedzany po jego pierwszym synu i przed pozostalymi
	if (wezel->synowie[0] != nullptr)
		inOrder(wezel->synowie[0]);
	cout << wezel->wartosc << " "; // wyswietlenie wartosci elementu
	if (wezel->synowie[1] != nullptr)
		inOrder(wezel->synowie[1]);
}

template <typename Typ>
void drzewo<Typ>::inOrder()
{
	inOrder(Korzen());
}

// funkcja zwracajaca wieksza wartosc z dwoch
template <typename Typ>
Typ max(Typ a, Typ b)
{
	if (a >= b)
		return a;
	else
		return b;
}

// funkcja zwracajaca wysokosc drzewa
template <typename Typ>
int wysokosc(drzewo<Typ> *tree, wezel<Typ> *node)
{
	if (node->czyLisc()) // sprawdzenie czy wezel jest lisciem
		return 0;
	else {
		int h = 0; // zmienna przechowujaca wysokosc
		if (node->synowie[0] != nullptr) {
			h = max(h, wysokosc(tree, node->synowie[0])); // wywolanie rekurencyjne dla lewego syna
		}
		if (node->synowie[1] != nullptr) {
			h = max(h, wysokosc(tree, node->synowie[1])); // wywolanie rekurencyjne dla prawego syna
		}
		return 1 + h; // zwiekszenie zmiennej przechowujacej wysokosc o 1
	}
}

// funkcja zwracajaca poziom wezla
template <typename Typ>
int poziomWezla(drzewo<Typ> *tree, wezel<Typ> *node)
{
	if (node == tree->Korzen()) // koniec gdy jest korzeniem
		return 0;
	return(1 + poziomWezla(tree, node->ojciec)); // wywolanie rekurencyjne jesli istnieje ojciec
}

int main()
{
	drzewo<int> *tree = new drzewo<int>;
	
	tree->dodaj(2, tree->Korzen());
	tree->dodaj(72, tree->Korzen()); 
	tree->dodaj(51, tree->Korzen());
	tree->dodaj(7, tree->Korzen()); 
	tree->dodaj(4, tree->Korzen()); 
	tree->dodaj(3, tree->Korzen()); 
	tree->dodaj(12, tree->Korzen());
	tree->dodaj(5, tree->Korzen());
	tree->dodaj(9, tree->Korzen());
	tree->dodaj(21, tree->Korzen());  
	tree->dodaj(13, tree->Korzen());

	//cout << tree->Korzen()->synowie[1]->synowie[1]->czyLisc() << endl;
	
	cout << "Wysokosc drzewa: " << wysokosc(tree, tree->Korzen());
	cout << "\nWartosc korzenia: " << tree->KorzenW();
	cout << "\npreOrder: ";
	tree->preOrder();
	cout << "\npostOrder: ";
	tree->postOrder();
	cout << "\ninOrder: ";
	tree->inOrder(); 

	tree->usun(51, tree->Korzen());

	cout << "\n\nPrezentacja usuwania elementow:";
	cout << "\npreOrder: ";
	tree->preOrder();
	cout << "\npostOrder: ";
	tree->postOrder();
	cout << "\ninOrder: ";
	tree->inOrder();
	cout << "\n";	
}