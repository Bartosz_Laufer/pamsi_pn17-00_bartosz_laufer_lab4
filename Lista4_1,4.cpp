#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

// deklaracje klas
template <typename Typ>
class kolejka; // klasa zawierajaca glowne metody kolejki
template <typename Typ>
class kolejkaPriorytetowa; // klasa z metodami kolejki priorytetowej

template <typename Typ>
class element {
private:
	Typ wartosc;
	Typ klucz; // priorytet danego elementu
	element *next; // wskaznik na kolejny element
	element *previous; // wskaznik na poprzedni element
	element(); // konstruktor
	friend kolejka<Typ>;
	friend kolejkaPriorytetowa<Typ>;
};

template <typename Typ>
element<Typ>::element() {
	next = nullptr;
	previous = nullptr;
}

template <typename Typ>
class kolejka {
public:
	void display(); // funkcja wyswietlajaca kolejke
	void usun(); // usuwanie elementu z najmniejszym kluczem
	void removeAll(); // usuwanie wszystkich elementow kolejki
	int size(); // zwraca aktualny rozmiar kolejki
	bool isEmpty(); // czy kolejka jest pusta
	Typ& front(); // zwraca element z przodu kolejki
	void dodaj(const Typ &wartosc); //funkcja dodajaca element na koniec kolejki
	kolejka(); // konstruktor
protected:
	int rozmiar; // aktualny rozmiar kolejki
	element<Typ> *first; // wskaznik na pierwszy element kolejki
	element<Typ> *last; // wskaznik na ostatni element kolejki
};

template <typename Typ>
kolejka<Typ>::kolejka() {
	first = nullptr;
	last = nullptr;
	rozmiar = 0;
}

template <typename Typ>
bool kolejka<Typ>::isEmpty()
{
	return !first; // zwraca prawde, jesli kolejka jest pusta
}

template <typename Typ>
int kolejka<Typ>::size()
{
	return rozmiar; // zwraca rozmiar kolejki
}

template <typename Typ>
Typ& kolejka<Typ>::front()
{
	if (!isEmpty())
		return first->wartosc;
	string EmptyQueueException = "Nie mozna pobrac elementu - kolejka jest pusta!\n";
	throw EmptyQueueException; // gdy kolejka pusta i nie ma czego zwrocic
}

template <typename Typ>
void kolejka<Typ>::display()
{
	if (isEmpty()) { // gdy kolejka pusta, wyswietlany jest komunikat
		cout << "Nie mozna wyswietlic kolejki - kolejka jest pusta!" << endl << endl;
	}
	else {
		if (first != nullptr) {
			element<Typ> *temp = first; // wskaznik na pierwszy element kolejka
			while (temp != nullptr) {
				cout << temp->wartosc << " ";
				temp = temp->next;  // "przelatuje" po kolei po wszystkich elementach
			}
			cout << endl;
		}
	}
}

template <typename Typ>
void kolejka<Typ>::usun()
{
	if (rozmiar > 0) { // sprawdzenie, czy kolejka nie jest pusta
		element<Typ> *temp = first;
		first = first->next; // nowy poczatek listy znajduje sie jeden element dalej
		delete temp;
		rozmiar--;
	}
	else
		cout << "Nie mozna usunac elementu - kolejka jest pusta!\n";
}

template <typename Typ>
void kolejka<Typ>::removeAll()
{
	while (first != nullptr) {
		usun(); // usuwanie elementu z najmniejszym kluczem dopoki kolejka nie bedzie pusta
	}
}

template <typename Typ>
void kolejka<Typ>::dodaj(const Typ &wartosc) {
	element<Typ> *nowy = new element<Typ>; // tworzenie nowego elementu
	nowy->wartosc = wartosc;
	nowy->klucz = wartosc;
	if (first == nullptr) { // jesli to pierwszy element , to zostaje jej poczatkiem
		first = nowy;
		last = first;
	}
	else {
		element<Typ> *temp = last;
		last->next = nowy; // ostatni element wskazuje na nowy
		last = nowy; // nowy element jest ostatnim w kolejce
		last->next = nullptr; // nowy element nie wskazuje na nic
	}
	rozmiar++;
}

// ********************KOLEJKA PRIOYTETOWA*********************
template <typename Typ>
class kolejkaPriorytetowa : public kolejka<Typ> {
public:
	void insert(const Typ &wartosc, Typ klucz); //dodaje element do kolejki
	void removeMin(); // usuwanie elementu z najmniejszym kluczem
	void removeAll(); // usuwanie wszystkich elementow kolejki
	kolejkaPriorytetowa(); // konstruktor
};

template <typename Typ>
kolejkaPriorytetowa<Typ>::kolejkaPriorytetowa()
{
	first = nullptr;
	last = nullptr;
	rozmiar = 0;
}

template <typename Typ>
void kolejkaPriorytetowa<Typ>::removeMin()
{
	if (first != nullptr) { // jesli kolejka nie jest pusta
		element<Typ> *temp = first;
		first = first->next; // przesuniecie przodu kolejki o jeden element dalej
		if (first == nullptr) // jesli kolejka jest pusta
			last = nullptr;
		else
			first->previous = nullptr; // previous nowego przodu kolejki nie wskazuje na nic
		delete temp;
		rozmiar--;
	}
	else
		cout << "Nie mozna usunac elementu - kolejka jest pusta!\n\n";
}

template <typename Typ>
void kolejkaPriorytetowa<Typ>::removeAll()
{
	while (first != nullptr) {
		removeMin(); // usuwanie elementu z najmniejszym kluczem dopoki kolejka nie bedzie pusta
	}
}

template <typename Typ>
void kolejkaPriorytetowa<Typ>::insert(const Typ &wartosc, Typ klucz)
{
	element<Typ> *nowy = new element<Typ>; // tworzenie nowego elementu
	nowy->wartosc = wartosc;
	nowy->klucz = klucz;
	if (first == nullptr) { // jesli kolejka jest pusta
		first = nowy;
		last = first;
	}
	else if (klucz < first->klucz) { // dodawanie na poczatek kolejki
		element<Typ> *temp = first;
		temp->previous = nowy;
		nowy->next = temp; // nowy element wskazuje na pierwszy
		first = nowy; // nowy element jest teraz pierwszym w kolejce
	}
	else {
		element<Typ> *temp = first;
		while (temp->next != nullptr && temp->next->klucz < klucz) { // "przegladanie" kolejki do momentu znalezienia elementu o wiekszym kluczu
			temp = temp->next;
		}
		element<Typ> *temp2 = new element<Typ>;
		temp2 = temp->next; // dodawanie B miedzy elementy A i C
		temp->next = nowy;  // A wskazuje na B
		nowy->next = temp2; // B wskazuje na C
		if (nowy->next == nullptr) { // jesli dodawanie na koniec kolejki (czyli C to nullptr)
			last = nowy;
			nowy->previous = temp; // B wskazuje na A
		}
		else { // jesli C to istniejacy element
			temp2->previous = nowy; // C wskazuje na B
			nowy->previous = temp; // B wskazuje na A
		}
	}
	rozmiar++;
}

// *********************************************************************************
template <typename Typ>
kolejkaPriorytetowa<Typ>* insertSort(kolejka<Typ> *Q1, kolejkaPriorytetowa<Typ> *Q2)
{
	for (int i = 0; i < 12; i++)
		Q1->dodaj(rand() % 50); // wypelnienie zwyklej kolejki losowymi liczbami
	cout << "Przed sortowaniem:\n";
	Q1->display(); // zawartosc kolejki przed sortowaniem

	while (!Q1->isEmpty()) {
		Q2->insert(Q1->front(), Q1->front()); // przepisanie elementu do kolejki priorytetowej (klucz = wartosc, by kolejka posortowala odpowiednio)
		Q1->usun(); // usuniecie elementu z kolejki zwyklej
	}
	return Q2;
}

int main()
{
	kolejkaPriorytetowa<int> *queue = new kolejkaPriorytetowa<int>;
	kolejka<int> *q = new kolejka<int>;
	queue=insertSort(q, queue);
	queue->insert(15, 15);
	queue->insert(3, 3);
	queue->display();

	/*
	cout << "\nPrzykladowe operacje na kolejce priorytetowej\n";
	queue->insert(2, 1);
	queue->display();
	queue->insert(3, 0);
	queue->display();
	queue->insert(4, 2);
	queue->insert(5, 3);
	queue->display();
	queue->insert(37, 5);
	queue->insert(16, 2);
	queue->insert(13, -1);
	queue->display();
	queue->removeMin();
	queue->removeMin();
	queue->display();
	queue->removeAll();
	try {
		cout << queue->front();
	}
	catch (string EmptyQueueException) {
		cerr << EmptyQueueException;
	}
	queue->insert(2, 3);
	queue->insert(5, 1);
	queue->insert(7, 5);
	queue->display();
	*/
}