#include <iostream>

using namespace std;

template <typename Typ>
class drzewoBin;

template <typename Typ>
class wezel {
public:
	Typ wartosc; // wartosc przechowywana przez wezel
	wezel *ojciec; // wskaznik na ojca
	wezel *lSyn; // wskaznik na lewego syna
	wezel *pSyn; // wskaznik na prawego syna
	int LczyP; // 0 gdy jest lewym synem, 1 gdy prawym
	bool czyLisc(); // sprawdza czy wezel jest lisciem
};

template <typename Typ>
bool wezel<Typ>::czyLisc()
{
	if (lSyn == nullptr && pSyn == nullptr)
		return true; // zwraca true gdy nie ma synow
	else
		return false; // zwraca falsz gdy ma co najmniej 1 syna
}

template <typename Typ>
class drzewoBin {
public:
	drzewoBin(); // konstruktor
	~drzewoBin(); // destruktor
	void usun(Typ wartosc); // usuwa wezel o danej wartosci
	void usunDrzewo(); // usuwa cale drzewo
	void dodaj(Typ wartosc); // dodaje wartosc do drzewa
	wezel<Typ>* Korzen(); // zwraca korzen drzewa
	Typ KorzenW(); // zwraca wartosc korzenia
	wezel<Typ>* min(wezel<Typ> *wezel); // szuka najmniejsza wartosc w drzewie
	void preOrder(); // przejscie drzewa wzdluzne
	void postOrder(); // przejscie drzewa wsteczne
	void inOrder(); // przejscie drzewa poprzecznie
private:
	wezel<Typ> *korzen; // wskaznik na korzen drzewa
	void usun(Typ wartosc, wezel<Typ> *lisc); // metoda pomocnicza do usuwania wezla
	void usunDrzewo(wezel<Typ> *lisc); // metoda pomocnicza do usuwania drzewa
	void dodaj(Typ wartosc, wezel<Typ> *lisc); // metoda pomocnicza do dodawania elementu
	void preOrder(wezel<Typ> *wezel); // metoda pomocnicza do przejscia drzewa
	void postOrder(wezel<Typ> *wezel); // metoda pomocnicza do przejscia drzewa
	void inOrder(wezel<Typ> *wezel); // metoda pomocnicza do przejscia drzewa
};

template <typename Typ>
drzewoBin<Typ>::drzewoBin()
{
	korzen = nullptr;
}

template <typename Typ>
drzewoBin<Typ>::~drzewoBin()
{
	usunDrzewo();
}

template <typename Typ>
wezel<Typ>* drzewoBin<Typ>::Korzen()
{
	return korzen; 
}

template <typename Typ>
Typ drzewoBin<Typ>::KorzenW()
{
	return korzen->wartosc;
}

template <typename Typ>
wezel<Typ>* drzewoBin<Typ>::min(wezel<Typ> *wezel)
{
	while (wezel->lSyn != nullptr) {
		wezel = wezel->lSyn;
		min(wezel);
	}
	return wezel;
}

template <typename Typ>
void drzewoBin<Typ>::usun(Typ wartosc, wezel<Typ> *lisc)
{
	if (lisc != nullptr) {
		if (wartosc == lisc->wartosc) { // znalezienie wezla do usuniecia
			if (lisc != Korzen() || (lisc==Korzen() && Korzen()->lSyn != nullptr && Korzen()->pSyn != nullptr)) {
				if (lisc->czyLisc()) { // gdy usuwany wezel jest lisciem
					if (lisc->LczyP == 0) // ojciec traci syna
						lisc->ojciec->lSyn = nullptr;
					else if (lisc->LczyP == 1)
						lisc->ojciec->pSyn = nullptr;
				}
				else if (lisc->lSyn != nullptr && lisc->pSyn != nullptr) { // gdy wezel ma 2 synow
					wezel<Typ> *nowy = min(lisc->pSyn); // znalezienie minimum z prawej strony wezla do usuniecia
					int temp = lisc->wartosc; // zamiana miejscami dwoch wartosci
					lisc->wartosc = nowy->wartosc;
					nowy->wartosc = temp;
					usun(wartosc, lisc->pSyn); // usuniecie danej wartosci juz z innego miejsca wezla
				}
				else { // gdy wezel ma jednego syna
					if (lisc->lSyn != nullptr) { // sprawdzenie czy ma lewego czy prawego syna
						lisc->lSyn->ojciec = lisc->ojciec; // nowym ojcem syna bedzie jego dziadek
						if (lisc->LczyP == 0)
							lisc->ojciec->lSyn = lisc->lSyn; // nowym synem dziadka bedzie jego wnuk
						else if (lisc->LczyP == 1)
							lisc->ojciec->pSyn = lisc->lSyn;
					}
					else {
						lisc->pSyn->ojciec = lisc->ojciec; // nowym ojcem syna bedzie jego dziadek
						if (lisc->LczyP == 0)
							lisc->ojciec->lSyn = lisc->pSyn; // nowym synem dziadka bedzie jego wnuk
						else if (lisc->LczyP == 1)
							lisc->ojciec->pSyn = lisc->pSyn;
					}
				}
			}
			else { // gdy do usuniecia jest korzen z 0 lub 1 synem
				if (Korzen()->lSyn == nullptr && Korzen()->pSyn == nullptr) // gdy korzen nie ma synow
					usunDrzewo();
				else if (Korzen()->lSyn != nullptr) { // gdy korzen ma tylko lewego syna
					korzen->lSyn->ojciec = nullptr;
					korzen = korzen->lSyn;
				}
				else { // gdy korzen ma tylko prawego syna
					korzen->pSyn->ojciec = nullptr; 
					korzen = korzen->pSyn;
				}
			}
		}
		else if (wartosc < lisc->wartosc)
			usun(wartosc, lisc->lSyn); // wywolanie funkcji dla lewego syna
		else
			usun(wartosc, lisc->pSyn); // wywolanie funkcji dla prawego syna
	}
	else
		cout << "Nie ma takiego elementu w drzewie!\n\n";
}

template <typename Typ>
void drzewoBin<Typ>::usun(Typ wartosc)
{
	usun(wartosc, Korzen());
}

template <typename Typ>
void drzewoBin<Typ>::usunDrzewo(wezel<Typ> *lisc)
{
	if (lisc != nullptr) { // sprawdza czy wezel jest lisciem
		usunDrzewo(lisc->lSyn); // dojscie do lisci drzewa
		usunDrzewo(lisc->pSyn);
		delete lisc;
	}
}

template <typename Typ>
void drzewoBin<Typ>::usunDrzewo()
{
	usunDrzewo(korzen);
}

template <typename Typ>
void drzewoBin<Typ>::dodaj(Typ wartosc, wezel<Typ> *lisc)
{
	if (wartosc < lisc->wartosc) { // sprawdza czy isc w lewo czy w prawo
		if (lisc->lSyn != nullptr) // sprawdza czy wezel ma synow
			dodaj(wartosc, lisc->lSyn); // zejscie do kolejnego wezla
		else {
			wezel<Typ> *nowy = new wezel<Typ>;
			nowy->wartosc = wartosc;
			nowy->LczyP = 0; // wezel jest lewym synem
			lisc->lSyn = nowy; // dodanie nowego elementu
			lisc->lSyn->ojciec = lisc; // ustawienie ojca nowego elementu
			lisc->lSyn->lSyn = nullptr; // ustawienie synow nowego wezla na null
			lisc->lSyn->pSyn = nullptr;
		}
	}
	else if (wartosc >= lisc->wartosc) { // gdy wartosc dodawana wieksza niz wartosc wezla
		if (lisc->pSyn != nullptr)
			dodaj(wartosc, lisc->pSyn); // zejscie do kolejnego wezla
		else {
			wezel<Typ> *nowy = new wezel<Typ>;
			nowy->wartosc = wartosc;
			nowy->LczyP = 1; // wezel jest prawym synem
			lisc->pSyn = nowy; // dodanie nowego elementu
			lisc->pSyn->ojciec = lisc; // ustawienie ojca nowego elementu
			lisc->pSyn->lSyn = nullptr; // ustawienie synow nowego wezla na null
			lisc->pSyn->pSyn = nullptr;
		}
	}
}

template <typename Typ>
void drzewoBin<Typ>::dodaj(Typ wartosc)
{
	if (korzen != nullptr) // gdy drzewo ma juz korzen
		dodaj(wartosc, korzen); 
	else { // gdy drzewo nie ma korzenia
		wezel<Typ> *nowy = new wezel<Typ>;
		nowy->wartosc = wartosc;
		korzen = nowy; // ustawienie nowej wartosci jako korzen
		korzen->lSyn = nullptr;
		korzen->pSyn = nullptr;
	}
}

template <typename Typ>
void drzewoBin<Typ>::preOrder(wezel<Typ> *wezel)
{ // wezel odwiedzany przed jego potomkami
	cout << wezel->wartosc << " "; // wyswietlenie wartosci elementu
	if (wezel->lSyn != nullptr) 
		preOrder(wezel->lSyn);
	if (wezel->pSyn != nullptr)
		preOrder(wezel->pSyn);
	//if (wezel->czyLisc())
		//cout << endl; // od nowej linii gdy natrafiono na lisc
}

template <typename Typ>
void drzewoBin<Typ>::preOrder()
{
	preOrder(Korzen());
}

template <typename Typ>
void drzewoBin<Typ>::postOrder(wezel<Typ> *wezel)
{ // wezel jest odwiedzany po jego potomkach
	if (wezel->lSyn != nullptr) 
		postOrder(wezel->lSyn);
	if (wezel->pSyn != nullptr)
		postOrder(wezel->pSyn);
	cout << wezel->wartosc << " "; // wyswietlenie wartosci elementu
}

template <typename Typ>
void drzewoBin<Typ>::postOrder()
{
	postOrder(Korzen());
}

template <typename Typ>
void drzewoBin<Typ>::inOrder(wezel<Typ> *wezel)
{ // wezel odwiedzany po jego lewym poddrzewie i przed prawym
	if (wezel->lSyn != nullptr) 
		inOrder(wezel->lSyn);
	cout << wezel->wartosc << " "; // wyswietlenie wartosci elementu
	if (wezel->pSyn != nullptr)
		inOrder(wezel->pSyn);
}

template <typename Typ>
void drzewoBin<Typ>::inOrder()
{
	inOrder(Korzen());
}

// funkcja zwracajaca wieksza wartosc z dwoch
template <typename Typ>
Typ max(Typ a, Typ b)
{
	if (a >= b)
		return a;
	else
		return b;
}

// funkcja zwracajaca wysokosc drzewa
template <typename Typ>
int wysokosc(drzewoBin<Typ> *tree, wezel<Typ> *node)
{
	if (node->czyLisc()) // sprawdzenie czy wezel jest lisciem
		return 0;
	else {
		int h = 0; // inicjalizacja zmiennej przechowujacej wysokosc drzewa
		if (node->lSyn != nullptr) {
			h = max(h, wysokosc(tree, node->lSyn)); // wywolanie rekurencyjne dla lewego syna
		}
		if (node->pSyn != nullptr) {
			h = max(h, wysokosc(tree, node->pSyn)); // wywolanie rekurencyjne dla prawego syna
		}
		return 1 + h; // zwiekszenie zmiennej przechowujacej wysokosc o 1
	}
}

// funkcja zwracajaca poziom wezla
template <typename Typ>
int poziomWezla(drzewoBin<Typ> *tree, wezel<Typ> *node)
{
	if (node == tree->Korzen())
		return 0;
	return (1 + poziomWezla(tree, node->ojciec)); // wywolanie rekurencyjne dla ojca wezla i zwiekszenie poziomu o 1
}

int main()
{
	drzewoBin<int> *tree = new drzewoBin<int>;
	
	tree->dodaj(3);
	tree->dodaj(5);
	tree->dodaj(9);
	tree->dodaj(8);
	tree->dodaj(0);
	tree->dodaj(1);
	tree->dodaj(2);
	tree->dodaj(4);
	tree->dodaj(7);
	tree->dodaj(11);
	tree->dodaj(10);
	tree->dodaj(6);
	cout << "Wysokosc drzewa: " << wysokosc(tree, tree->Korzen());
	cout << "\nWartosc korzenia: " << tree->KorzenW();
	cout << "\npreOrder: ";
	tree->preOrder();
	cout << "\npostOrder: ";
	tree->postOrder();
	cout << "\ninOrder: ";
	tree->inOrder();

	tree->usun(6);
	cout << "\n\nWysokosc drzewa: " << wysokosc(tree, tree->Korzen());
	cout << "\nWartosc korzenia: " << tree->KorzenW();
	cout << "\npreOrder: ";
	tree->preOrder();
	cout << "\npostOrder: ";
	tree->postOrder();
	cout << "\ninOrder: ";
	tree->inOrder();

	tree->usun(3);
	cout << "\n\nWysokosc drzewa: " << wysokosc(tree, tree->Korzen());
	cout << "\nWartosc korzenia: " << tree->KorzenW();
	cout << "\npreOrder: ";
	tree->preOrder();
	cout << "\npostOrder: ";
	tree->postOrder();
	cout << "\ninOrder: ";
	tree->inOrder();
}